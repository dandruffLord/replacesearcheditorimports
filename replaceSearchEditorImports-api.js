// Regex-ed input:
// search by regex: (?:\.|(src)|~)/api/
// exclude files by globs: src/archive, src/api, (library)
// implicitly exclude files by globs: mock_server, coverage, dist, node_modules

const fs = require("fs");

const importExceptionList = ["orders", "payment"];
const searchEditorImports = fs.readFileSync("searchEditorImports.txt", "utf8");
const newSearchEditorImports = searchEditorImports.replaceAll(
  /(?:~\/|(?:\.{1,2}\/)*(?:src\/)?)api\/([^'\n]+)/g,
  (match, p1) =>
    importExceptionList.some((importException) =>
      p1.startsWith(importException)
    )
      ? match
      : `@academysports/api-lib/${p1}`
);
fs.writeFileSync("newSearchEditorImports.txt", newSearchEditorImports);
