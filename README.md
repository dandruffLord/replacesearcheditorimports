Repository to house JS files for mutating imports of candidate ui-monorepo workspaces (src/api, src/utils) en masse, 
as well as other files to also help facilitate the adoption of a multi-app structure for ui-monorepo.
Follow this link for a guide on how to use replaceSearchEditorImports.js: https://academysports.atlassian.net/wiki/spaces/DD/pages/3163029558/Monorepo+Implementation+Details#Auto-Fixing-Registry%2FLibrary-Import-Statements.
Then for the other files, if the sparing documentation and hopefully intuitive code fails to clear doubts on their method of use,
I recommend attempting a dry run using them on ui-monorepo.
