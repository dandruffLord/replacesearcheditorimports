// Regex-ed input:
// search by string: import
// include files by glob: {boundaryAbsPath}**/*.js
const fs = require("fs");
const path = require("path");

// Change this if you want to use a different boundary absolute path
const boundaryAbsPath = "src/utils/";

const importsFromInsideBoundary = [];
const importsFromOutsideBoundary = [];
const importsFromNodeModules = [];
const firstRegex = /(?<=\n)(\S+?):(.+?)(?=\n(?:\S|$))/gs;
const searchEditorImports = fs.readFileSync("searchEditorImports.txt", "utf8");
const resolveIfAliasPath = (aliasPath) =>
  (aliasPath[0] === "~" && `src${aliasPath.slice(1)}`) ||
  (aliasPath[0] !== "." && `${boundaryAbsPath}node_modules/${aliasPath}`) ||
  false;
const processFilePathAndFileContentMatches = ([
  _match,
  filePath,
  fileContent,
]) => {
  const targetAbsPath = filePath.replaceAll("\\", "/");
  const secondRegex = /import.+?from '(.+?)';/gs;
  const processImportPathMatches = ([_match, importPath]) => {
    const sourceAbsPath =
      resolveIfAliasPath(importPath) ||
      path.posix.join(path.posix.dirname(targetAbsPath), importPath);
    const importText = `${targetAbsPath} <-- ${sourceAbsPath}`;
    sourceAbsPath.startsWith(`${boundaryAbsPath}node_modules`)
      ? importsFromNodeModules.push(importText)
      : sourceAbsPath.startsWith(boundaryAbsPath)
      ? importsFromInsideBoundary.push(importText)
      : importsFromOutsideBoundary.push(importText);
  };
  [...fileContent.matchAll(secondRegex)].forEach(processImportPathMatches);
};
[...searchEditorImports.matchAll(firstRegex)].forEach(
  processFilePathAndFileContentMatches
);
console.log(
  `Imports from inside ${boundaryAbsPath}:`,
  importsFromInsideBoundary
);
console.log(
  `Imports from outside ${boundaryAbsPath}:`,
  importsFromOutsideBoundary
);
console.log(`Imports from node_modules:`, importsFromNodeModules);
