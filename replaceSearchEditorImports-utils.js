// Regex-ed input:
// search by regex: (?:\.|(src)|~)/utils/
// exclude files by globs: src/archive, src/utils, (library)
// implicitly exclude files by globs: mock_server, coverage, dist, node_modules

const fs = require("fs");

const importExceptionList = ["gcUtils", "placeOrderUtils"];
const searchEditorImports = fs.readFileSync("searchEditorImports.txt", "utf8");
const newSearchEditorImports = searchEditorImports.replaceAll(
  /(?:~\/|(?:\.{1,2}\/)*(?:src\/)?)utils\/([^'\n]+)/g,
  (match, p1) =>
  importExceptionList.includes(p1)
      ? match
      : `@academysports/shared-utils-lib/${p1}`
);
fs.writeFileSync("newSearchEditorImports.txt", newSearchEditorImports);
