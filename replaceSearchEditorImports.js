// Regex-ed input:
// search by string: @academysports/ui-component-library/dist
// exclude files by globs: src/archive, (library)
// implicitly exclude files by globs: mock_server, coverage, dist, node_modules

const fs = require("fs");
const buildEntries = require("./build.entries");

const searchEditorImports = fs.readFileSync("searchEditorImports.txt", "utf8");
const newSearchEditorImports = searchEditorImports.replaceAll(
  /(?<=@academysports\/)ui-component-library\/dist\/+([^']+)/g,
  (_, p1) =>
    `ui-component-lib/${buildEntries[p1]
      .replace(/^(\.\/)/, "")
      .replace(/(index)?(.js)?$/, "")}`
);
fs.writeFileSync("newSearchEditorImports.txt", newSearchEditorImports);
